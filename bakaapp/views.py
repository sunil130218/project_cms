from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin
# these are defined function import paxi ko
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .forms import *
from django.shortcuts import render, redirect
from django.views.generic import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import *

# Clientsite Curd---------------------------------------------------------


class ClientHomeView(TemplateView):
    template_name = 'clienttemplates/clienthome.html'


class ClientServiceListView(ListView):
    template_name = "clienttemplates/clientservicelist.html"
    model = Service
    context_object_name = 'clientservicelist'


class ClientServiceDetailView(DetailView):
    template_name = 'clienttemplates/clientservicedetail.html'
    model = Service
    context_object_name = 'servicedetail'


class ClientServicePackageListView(ListView):
    template_name = "clienttemplates/clientservicepackagelist.html"
    model = ServicePackage
    context_object_name = 'clientservicepackagelist'


class ClientServicePackageDetailView(DetailView):
    template_name = 'clienttemplates/clientservicepackagedetail.html'
    model = ServicePackage
    context_object_name = 'servicepackagedetail'


class ClientOfferListView(ListView):
    template_name = 'clienttemplates/clientofferlistview'
    model = Offer
    context_object_name = 'offerlist'


class ClientOfferDetailView(DetailView):
    template_name = 'clienttemplates/clientofferdetail.html'
    model = Offer
    context_object_name = 'offerdetail'


class ClientServicePackageBookingView(CreateView):
    template_name = 'clienttemplates/clientservicepackagebooking.html'
    form_class = ServicePackageBookingForm
    success_url = reverse_lazy('bakaapp:clienthome')

    def form_valid(self, form):
        package_id = self.kwargs["pk"]
        package = ServicePackage.objects.get(id=package_id)

        form.instance.package = package

        return super().form_valid(form)


# Admin Site Curd--------------------------------------------------------


class AdminLoginView(FormView):
    template_name = 'admintemplates/adminlogin.html'
    form_class = AdminLoginForm
    success_url = reverse_lazy('bakaapp:adminhome')

    def form_valid(self, form):
        a = form.cleaned_data["username"]
        b = form.cleaned_data["password"]
        user = authenticate(username=a, password=b)

        if user is not None:
            login(self.request, user)

        else:
            return render(self.request, "admintemplates/adminhome.html", {"error": "Invalid Username Or Password", "form": form})

        return super().form_valid(form)


class AdminLogoutView(View):
    def get(self, request):
        logout(request)

        return redirect("/admin/login/")


class AdminRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            pass
        else:
            return redirect("/med-admin/login/")

        return super().dispatch(request, *args, **kwargs)


class AdminHomeView(AdminRequiredMixin, TemplateView):
    template_name = 'admintemplates/adminhome.html'

# admin service


class AdminServiceCreate(AdminRequiredMixin, CreateView):
    template_name = 'admintemplates/adminservicecreate.html'
    form_class = ServiceForm
    success_url = reverse_lazy('bakaapp:adminservicelist')


class AdminServiceUpdateView(AdminRequiredMixin, UpdateView):
    template_name = 'admintemplates/adminservicecreate.html'
    model = Service
    form_class = ServiceForm
    success_url = reverse_lazy('bakaapp:adminservicelist')


class AdminServiceListView(AdminRequiredMixin, ListView):
    template_name = 'admintemplates/adminservicelist.html'
    queryset = Service.objects.all().order_by('-id')
    context_object_name = "adminservicelist"


class AdminServiceDeleteView(AdminRequiredMixin, DeleteView):
    template_name = 'admintemplates/adminservicedelete.html'
    model = Service
    success_url = reverse_lazy('bakaapp:adminservicelist')

# admin service package


class AdminServicePackageCreateView(AdminRequiredMixin, CreateView):
    template_name = 'admintemplates/adminservicepackagecreate.html'
    form_class = ServicePackageForm
    success_url = reverse_lazy('bakaapp:adminservicepackagelist')


class AdminServicePackageListView(AdminRequiredMixin, ListView):
    template_name = 'admintemplates/adminservicepackagelist.html'
    queryset = ServicePackage.objects.all().order_by('-id')
    context_object_name = "adminservicepackagelist"


class AdminServicePackageUpdateView(AdminRequiredMixin, UpdateView):
    template_name = 'admintemplates/adminservicecreate.html'
    model = ServicePackage
    form_class = ServicePackageForm
    success_url = reverse_lazy('bakaapp:adminservicepackagelist')


class AdminServicePackageDeleteView(AdminRequiredMixin, DeleteView):
    template_name = 'admintemplates/adminservicepackagedelete.html'
    model = ServicePackage
    success_url = reverse_lazy('bakaapp:adminservicepackagelist')

# admin album


class AdminAlbumCreateView(AdminRequiredMixin, CreateView):
    template_name = 'admintemplates/adminalbumcreate.html'
    form_class = AdminAlbumForm
    success_url = reverse_lazy('bakaapp:adminalbumlist')


class AdminAlbumListView(AdminRequiredMixin, ListView):
    template_name = 'admintemplates/adminalbumlist.html'
    queryset = ServicePackage.objects.all().order_by('-id')
    context_object_name = "adminalbumlist"


class AdminAlbumUpdateView(AdminRequiredMixin, UpdateView):
    template_name = 'admintemplates/adminalbumcreate.html'
    model = Album
    form_class = AdminAlbumForm
    success_url = reverse_lazy('bakaapp:adminalbumlist')


class AdminAlbumDeleteView(AdminRequiredMixin, DeleteView):
    template_name = 'admintemplates/adminalbumdelete.html'
    model = Album
    success_url = reverse_lazy('bakaapp:adminalbumlist')

# admin offer curd


class AdminOfferCreateView(AdminRequiredMixin, CreateView):
    template_name = 'admintemplates/adminoffercreate.html'
    form_class = AdminOfferForm
    success_url = reverse_lazy('bakaapp:adminofferlist')


class AdminOfferUpdateView(AdminRequiredMixin, UpdateView):
    template_name = 'admintemplates/adminoffercreate.html'
    model = Offer
    form_class = AdminOfferForm
    success_url = reverse_lazy('bakaapp:adminofferlist')


class AdminOfferListView(AdminRequiredMixin, ListView):
    template_name = 'admintemplates/adminofferlist.html'
    queryset = Offer.objects.all().order_by('-id')
    context_object_name = "adminofferlist"


class AdminOfferDeleteView(AdminRequiredMixin, DeleteView):
    template_name = 'admintemplates/adminofferdelete.html'
    model = Offer
    success_url = reverse_lazy('bakaapp:adminofferlist')

# admin team curd


class AdminTeamCreateView(AdminRequiredMixin, CreateView):
    template_name = 'admintemplates/adminteamcreate.html'
    form_class = TeamForm
    success_url = reverse_lazy('bakaapp:adminteamlist')


class AdminTeamListView(AdminRequiredMixin, ListView):
    template_name = 'admintemplates/adminteamlist.html'
    queryset = Team.objects.all().order_by('-id')
    context_object_name = "adminteamlist"


class AdminTeamUpdateView(AdminRequiredMixin, UpdateView):
    template_name = 'admintemplates/adminteamcreate.html'
    model = Team
    form_class = TeamForm
    success_url = reverse_lazy('bakaapp:adminteamlist')


class AdminTeamDeleteView(AdminRequiredMixin, DeleteView):
    template_name = 'admintemplates/adminteamdelete.html'
    model = Team
    success_url = reverse_lazy('bakaapp:adminteamlist')
