from django.contrib.auth.models import User
from django import forms
from .models import *


class AdminLoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your username...'
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your password...'
    }))


class ServiceForm(forms.ModelForm):
    class Meta:
        model = Service
        fields = "__all__"
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter your service title here......',
            }),

            'slug': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter slug...',
            }),

            'image': forms.ClearableFileInput(attrs={
                'class': 'from-control',

            }),

            'icon': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter icon...',
            }),

            'content': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Enter content ...',
            }),
        }


class ServicePackageForm(forms.ModelForm):
    class Meta:
        model = ServicePackage
        fields = "__all__"
        widgets = {
            'service': forms.Select(attrs={
                'class': 'form-control',
                'placeholder': 'Enter required service',
            }),

            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter title....',
            }),
            'rate': forms.NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter rate....',
            }),
            'duration': forms.NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter duration here...',
            }),

            'details': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Enter content ...',

            }),
            'image': forms.FileInput(attrs={
                'class': 'from-control',

            }),


        }


class AdminAlbumForm(forms.ModelForm):
    class Meta:
        model = Album
        fields = "__all__"
        widgets = {

            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter title....',
            }),

            'details': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter details....',
            }),

        }


class AdminOfferForm(forms.ModelForm):
    class Meta:
        model = Offer
        fields = "__all__"
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter new offer',
            }),

            'offer_starts': forms.DateTimeInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter offer starting date....',
            }),
            'offer_ends': forms.DateTimeInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter offer ending date....',
            }),
            'packages': forms.SelectMultiple(attrs={
                'class': ' sunil',
                'placeholder': 'select required package...',
            }),

            'details': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Enter content ...',

            }),
            'image': forms.FileInput(attrs={
                'class': 'form-control',

            }),

            'cost': forms.NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter required cost'
            }),


        }


class ServicePackageBookingForm(forms.ModelForm):
    class Meta:
        model = ServicePackageBooking
        exclude = ["package"]
        widgets = {
            'package': forms.Select(attrs={
                'class': 'form-control',
                'placeholder': 'select your fav package',
            }),

            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'your good name',
            }),
            'email': forms.EmailInput(attrs={
                'class': 'form-control',
                'placeholder': 'email example@gmail.com ....',
            }),
            'phone': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'contact phone number........',
            }),

            'date': forms.DateInput(attrs={
                'class': 'form-control',
                'placeholder': 'enter your favourable date ...',

            }),
            'adults': forms.NumberInput(attrs={
                'class': 'from-control',
                'placeholder': 'number of person....',

            }),
            'kids': forms.NumberInput(attrs={
                'class': 'from-control',
                'placeholder': 'number of kids....',

            }),
            'message': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'leave us message........',
            }),
            'address': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'your temporary address........',
            }),



        }


class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = "__all__"
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'your good name....',
            }),
            'post': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'your designation',
            }),

            'image': forms.FileInput(attrs={
                'class': 'from-control',

            }),

            'mobile': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'your mobile number ...',
            }),

            'email': forms.EmailInput(attrs={
                'class': 'form-control',
                'placeholder': ' example@gmail.com ......',

            }),

            'experience': forms.NumberInput(attrs={
                'class': 'from-control',
                'placeholder': ' experience period....',

            }),


        }
