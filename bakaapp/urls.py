from django.urls import path
from .views import *


app_name = "bakaapp"
urlpatterns = [
    path('', ClientHomeView.as_view(), name="clienthome"),
    path('med-client/service/list',
         ClientServiceListView.as_view(), name="clientservicelist"),
    path('med-client/service/<int:pk>/detail',
         ClientServiceDetailView.as_view(), name="clientservicedetail"),
    path('med-client/servicepackages/list',
         ClientServicePackageListView.as_view(),
         name="clientservicepackagelist"),
    path('med-client/servicepackage/<int:pk>/detail',
         ClientServicePackageDetailView.as_view(),
         name="clientservicepackagedetail"),
    path('med-client/offer/list',
         ClientOfferListView.as_view(),
         name="clientofferlist"),
    path('med-client/offer/<int:pk>/detail',
         ClientOfferDetailView.as_view(),
         name="clientofferdetail"),
    path('servicepackage/<int:pk>/book/',
         ClientServicePackageBookingView.as_view(),
         name="clientservicepackagebooking"),


    # admin login
    path('med-admin/login/', AdminLoginView.as_view(), name="adminlogin"),
    path('med-admin/logout/', AdminLogoutView.as_view(), name="adminlogout"),
    # admin service
    path('med-admin/home/', AdminHomeView.as_view(), name="adminhome"),
    path('med-admin/service/create/',
         AdminServiceCreate.as_view(), name="adminservicecreate"),
    path('med-admin/service/list/', AdminServiceListView.as_view(),
         name="adminservicelist"),
    path('med-admin/service/<int:pk>/update/',
         AdminServiceUpdateView.as_view(), name="adminserviceupdate"),
    path('med-admin/service/<int:pk>/delete/',
         AdminServiceDeleteView.as_view(), name="adminservicedelete"),
    # admin servicepackage
    path('med-admin/servicepackage/list/',
         AdminServicePackageListView.as_view(),
         name="adminservicepackagelist"),
    path('med-admin/servicepackage/create/',
         AdminServicePackageCreateView.as_view(),
         name='adminservicepackagecreate'),
    path('med-admin/servicepackage/<int:pk>/update/',
         AdminServicePackageUpdateView.as_view(),
         name="adminservicepackageupdate"),
    path('med-admin/servicepackage/<int:pk>/delete/',
         AdminServicePackageDeleteView.as_view(),
         name="adminservicepackagedelete"),
    # admin offer
    path('med-admin/offer/create/',
         AdminOfferCreateView.as_view(), name="adminoffercreate"),
    path('med-admin/offer/list/', AdminOfferListView.as_view(),
         name="adminofferlist"),
    path('med-admin/offer/<int:pk>/update/',
         AdminOfferUpdateView.as_view(), name="adminofferupdate"),
    path('med-admin/offer/<int:pk>/delete/',
         AdminOfferDeleteView.as_view(), name="adminofferdelete"),
    # admin album
    path('med-admin/album/create/',
         AdminAlbumCreateView.as_view(), name="adminalbumcreate"),
    path('med-admin/album/list/', AdminAlbumListView.as_view(),
         name="adminalbumlist"),
    path('med-admin/album/<int:pk>/update/',
         AdminAlbumUpdateView.as_view(), name="adminalbumupdate"),
    path('med-admin/album/<int:pk>/delete/',
         AdminAlbumDeleteView.as_view(), name="adminalbumdelete"),
    # admin team
    path('med-admin/team/create/',
         AdminTeamCreateView.as_view(), name="adminteamcreate"),
    path('med-admin/team/list/', AdminTeamListView.as_view(),
         name="adminteamlist"),
    path('med-admin/team/<int:pk>/update/',
         AdminTeamUpdateView.as_view(), name="adminteamupdate"),
    path('med-admin/team/<int:pk>/delete/',
         AdminTeamDeleteView.as_view(), name="adminteamdelete"),





]
