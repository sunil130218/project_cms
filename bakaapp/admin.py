from django.contrib import admin
from .models import *


admin.site.register([Organization, Service, ServicePackage,
                     Offer, Team, Testimonial, Album, Image, Video, Enquiry, Message, ServicePackageBooking, ])
