from django.db import models
from django.contrib.auth.models import User


class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Organization(TimeStamp):
    name = models.CharField(max_length=40)
    logo = models.ImageField(upload_to='logos')
    email = models.EmailField()
    phone = models.CharField(max_length=40)
    mobile = models.CharField(max_length=40, null=True, blank=True)
    address = models.CharField(max_length=40)
    slogan = models.CharField(max_length=200, null=True, blank=True)
    about = models.TextField()
    mission_and_vision = models.TextField()
    profile_image = models.ImageField(upload_to='organizations')
    website = models.CharField(max_length=40)
    map_location = models.CharField(max_length=40)
    favicon = models.ImageField(upload_to='favicons', null=True, blank=True)
    facebook = models.CharField(max_length=40, null=True, blank=True)
    instagram = models.CharField(max_length=40, null=True, blank=True)
    youtube = models.CharField(max_length=40, null=True, blank=True)
    customers_served = models.IntegerField(null=True, blank=True)
    awards = models.IntegerField(null=True, blank=True)
    estb_date = models.IntegerField(null=True, blank=True)

    def __str__(self):
        self.name


class Service(TimeStamp):
    title = models.CharField(max_length=40)
    slug = models.SlugField(unique=True)
    image = models.ImageField(upload_to='services')
    icon = models.CharField(max_length=20)
    content = models.TextField()

    def __str__(self):
        return self.title


class ServicePackage(TimeStamp):
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    title = models.CharField(max_length=40)
    rate = models.DecimalField(max_digits=19, decimal_places=2)
    duration = models.IntegerField(null=True, blank=True)
    details = models.TextField()
    image = models.ImageField(upload_to='servicepackages')

    def __str__(self):
        return self.title + '(' + self.service.title + ')'


class ServicePackageBooking(TimeStamp):
    package = models.ForeignKey(ServicePackage, on_delete=models.CASCADE)
    name = models.CharField(max_length=40)
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(max_length=20)
    address = models.CharField(max_length=100)
    date = models.DateField()
    adults = models.PositiveIntegerField(default=1)
    kids = models.PositiveIntegerField(default=0)
    message = models.TextField()

    def __str__(self):
        return self.name + '(' + self.package.title + ')'


class Offer(TimeStamp):
    title = models.CharField(max_length=20)
    offer_starts = models.DateTimeField(auto_now=True)
    offer_ends = models.DateTimeField(auto_now=True)
    packages = models.ManyToManyField(ServicePackage)
    details = models.TextField()
    image = models.ImageField(upload_to='offers')
    cost = models.DecimalField(max_digits=20, decimal_places=2)

    def __str__(self):
        return self.title


class Team(TimeStamp):
    name = models.CharField(max_length=40)
    post = models.CharField(max_length=40)
    image = models.ImageField(upload_to='teams')
    mobile = models.CharField(max_length=40)
    email = models.EmailField(null=True, blank=True)
    experience = models.PositiveIntegerField(null=True, blank=True)


class Testimonial(TimeStamp):
    name = models.CharField(max_length=40)
    photo = models.ImageField(upload_to='testimonials')
    sayings = models.TextField()
    profession = models.CharField(max_length=40)


class Album(TimeStamp):
    title = models.CharField(max_length=20)
    details = models.TextField()


class Image(TimeStamp):
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='images')
    caption = models.CharField(max_length=50)


class Video(TimeStamp):
    title = models.CharField(max_length=40)
    link = models.CharField(max_length=50)
    description = models.TextField()


class Enquiry(TimeStamp):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    mobile = models.CharField(max_length=50)
    subject = models.ForeignKey(Service, on_delete=models.CASCADE)
    message = models.TextField()


class Message(TimeStamp):
    sender = models.CharField(max_length=50)
    mobile = models.CharField(max_length=50)
    email = models.EmailField()
    subject = models.CharField(max_length=100)
    message = models.TextField()
